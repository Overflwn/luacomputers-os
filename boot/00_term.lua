--LCOS term API
--TODO: Letters don't get loaded; check that up

--TODO: Letters get listed but it fails at _reading_ any file
--NOTE: RES: 45x20

local colors = require("colors.lua")

local pixel_width, pixel_height = 8, 8

local currentFont = {}
local font = "default"
local fileList =  _filesystem.list("/sys/fonts/"..font..".font")
print("Letters: "..tostring(#fileList))
for each, file in ipairs(fileList) do
	print("Opening letter: ".."/sys/fonts/"..font..".font/"..file)
	local f, err = _filesystem.open("/sys/fonts/"..font..".font/"..file, "r")
	if not f then print("FAILED TO OPEN "..file.." : "..tostring(err)) end
	local theLine = ""
	local counter = 1
	local letterName = ""
	repeat
		theLine = f:read("*l")
		if theLine ~= nil then
			if counter == 1 then
				letterName = theLine
				if letterName == "" then letterName = " " end
				currentFont[letterName] = {}
				counter = counter+1

			else
				currentFont[letterName][counter-1] = {}
				for x=1, 8 do
					local dat = string.sub(theLine, x, x)
					table.insert(currentFont[letterName][counter-1], tostring(dat))
				end
				counter = counter+1
			end
		else
			print("<eof> reached")
		end
	until theLine == nil
	print("LOADED: "..tostring(file))
end

print("Font length: "..tostring(#currentFont.A))


local term = {}
term.width = 40
term.height = 25
term.x = 1
term.y = 1
term.cX = 1
term.cY = 1
term.colors = {
	["0"] = colors.black,
	["1"] = colors.orange,
	["2"] = colors.magenta,
	["3"] = colors.lightblue,
	["4"] = colors.yellow,
	["5"] = colors.lime,
	["6"] = colors.pink,
	["7"] = colors.gray,
	["8"] = colors.lightgray,
	["9"] = colors.cyan,
	["a"] = colors.purple,
	["b"] = colors.blue,
	["c"] = colors.brown,
	["d"] = colors.green,
	["e"] = colors.red,
	["f"] = colors.white,
	["x"] = colors.transparent
}
term.col_bg = "0"
term.col_fg = "f"
term.pixels = {}

for i=1, term.height do
	term.pixels[i] = {
		bg = "",
		fg = "",
		chars = "",
	}
	for x=1, term.width do
		term.pixels[i].bg = term.pixels[i].bg..term.col_bg
		term.pixels[i].fg = term.pixels[i].fg..term.col_fg
		term.pixels[i].chars = term.pixels[i].chars.." "
	end
end

function term.clear(self)
	--Pass a terminal object into this function (or call term:clear())
	--NOTE: Terminal object aren't currently able to be created, will do that later on
	for i=1, self.height do
		self.pixels[i] = {
			bg = "",
			fg = "",
			chars = "",
		}
		for x=1, self.width do
			self.pixels[i].bg = self.pixels[i].bg..self.col_bg
			self.pixels[i].fg = self.pixels[i].fg..self.col_fg
			self.pixels[i].chars = self.pixels[i].chars.." "
		end
	end
	--Clear terminal
	_gpu.setColor(term.colors[self.col_bg])
	local totalX = pixel_width * self.x - pixel_width + 1
	local totalY = pixel_height * self.y - pixel_height + 1
	local totalWidth = pixel_width * self.width
	local totalHeight = pixel_height * self.height
	_gpu.clearArea(totalX, totalY, totalWidth, totalHeight)
end

function term.setBgColor(self, col)
	if term.colors[tostring(col)] ~= nil then
		self.col_bg = col
	end
end

function term.setFgColor(self, col)
	if term.colors[tostring(col)] ~= nil then
		self.col_fg = col
	end
end

function term.getBgColor(self)
	return self.col_bg
end

function term.getFgColor(self)
	return self.col_fg
end

function term.setCursorPos(self, x, y)
	self.cX = x
	self.cY = y
end

function term.getCursorPos(self)
	return self.cX, self.cY
end

function term.redraw(self)
	for y=1, self.height do
		local pixel = self.pixels[y]
		for x=1, self.width do
			local theChar = string.sub(pixel.chars, x, x)
			local theBg = string.sub(pixel.bg, x, x)
			local theFg = string.sub(pixel.fg, x, x)
			local charData = currentFont[theChar]
			--In case that the char doesnt exist, skip this iteration
			if charData then
				_gpu.setColor(term.colors[theBg])
				local totalX = pixel_width*x-pixel_width+1
				local totalY = pixel_height*y-pixel_height+1
				--print("Sending: " .. totalX .. " AND " ..totalY)
				_gpu.clearArea(totalX, totalY, pixel_width, pixel_height)
				--print("SENT")
				_gpu.setColor(term.colors[theFg])
				for py=1, pixel_height do
					for px=1, pixel_width do
						if not charData[py] then print("CHR: "..py) end
						if charData[py][px] == "1" then
							_gpu.setPixel(totalX+px-1, totalY+py-1)
						end
					end
				end
			end
		end
	end
end

function term.getSize(self)
	return self.width, self.height
end


function term.write(self, text)
	local length = #text
	local finalText = text
	local cpos = self.cX
	if self.cX < 1 then
		finalText = string.sub(text, math.abs(self.cX)+1)
		length = #finalText
		cpos = 1
	end
	if cpos+length-1 > self.width then
		local diff = cpos+length-1 - self.width
		finalText = string.sub(finalText, 1, length-diff)
		length = #finalText
	end
	if self.cY >= 1 and self.cY <= self.height then
		local preChars = ""
		local preBg = ""
		local preFg = ""
		local afterChars
		local afterBg
		local afterFg
		local newFg = ""
		local newBg = ""
		for i=1, length do
			newFg = newFg..self.col_fg
			newBg = newBg..self.col_bg
		end

		if cpos > 1 then
			preChars = string.sub(self.pixels[self.cY].chars, 1, cpos-1)
			preBg = string.sub(self.pixels[self.cY].bg, 1, cpos-1)
			preFg = string.sub(self.pixels[self.cY].fg, 1, cpos-1)
		else
			preChars = ""
			preBg = ""
			preFg = ""
		end

		if cpos+length-1 < self.width then
			afterChars = string.sub(self.pixels[self.cY].chars, cpos+length)
			afterBg = string.sub(self.pixels[self.cY].bg, cpos+length)
			afterFg = string.sub(self.pixels[self.cY].fg, cpos+length)
		else
			afterChars = ""
			afterBg = ""
			afterFg = ""
		end
		self.pixels[self.cY].chars = preChars..text..afterChars
		self.pixels[self.cY].bg = preBg..newBg..afterBg
		self.pixels[self.cY].fg = preFg..newFg..afterFg
		self.cX = cpos+length
	end
	self:redraw()
end

term:write("L / ( ) [ ] $ ! ? . , ' \" = < >")
term:setCursorPos(1, 3)
term:write("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
term:redraw()
_G.term = term
