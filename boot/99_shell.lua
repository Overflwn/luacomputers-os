--LCOS: Shell
--Author: Piorjade

local colors = require("colors")

local _ver = 0.1
local shell = {}

term:clear()
term:setCursorPos(1,1)
term:write("LuaComputers OS ver.: "..tostring(_ver))
term:setCursorPos(1,2)
term:write("Enter the path to a file to execute it.")
term:setCursorPos(1,3)
term:write("> ")

function blinker()
	local timer = nil
	local toggled = false
	local oldX, oldY = term:getCursorPos()
	while true do
		sleep(0.5)
		oldX, oldY = term:getCursorPos()
		if toggled then
			term:write("_")
			term:setCursorPos(oldX, oldY)
			toggled = false
		else
			term:write(" ")
			term:setCursorPos(oldX, oldY)
			toggled = true
		end
	end
end

function reader()
	while true do
		local ev, btn = coroutine.yield()
		if ev == "char" then
			term:write(tostring(btn))
		end
	end
end

function shell.run()
print("Creating coroutines.."..tostring(coroutine.create).." | "..tostring(blinker))
	local c1, err = coroutine.create(blinker)
	if not c1 then return print("BLINKER: "..tostring(err)) end
	local c2, err = coroutine.create(reader)
	if not c2 then return print("READER: "..tostring(err)) end
	local ev, btn, x, y = nil, nil, nil, nil
	print("CREATED")
	while true do
		local ok, err = nil, nil
		ok, err = coroutine.resume(c1, ev, btn, x, y)
		if not ok then return print("INTERNAL RESUME C1: "..tostring(err)) end
		ok, err = coroutine.resume(c2, ev, btn, x, y)
		if not ok then return print("INTERNAL RESUME C2: "..tostring(err)) end
		ev, btn, x, y = coroutine.yield()
	end
end

_G.shell = shell