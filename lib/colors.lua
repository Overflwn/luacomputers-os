--Color library

white = 1
orange = 2
magenta = 4
lightblue = 8
yellow = 16
lime = 32
pink = 64
gray = 128
lightgray = 256
cyan = 512
purple = 1024
blue = 2048
brown = 4069
green = 8192
red = 16384
black = 32768
transparent = 65536