--[[
		LCOS
	
	The minimal example OS for LuaComputers.
	
	AUTHOR: Piorjade
]]

local _ver = 0.1
print(tostring(_filesystem.list("/boot")))
--Load core libraries
for each, library in ipairs(_filesystem.list("/boot")) do
	local fLib, sErr = loadfile("/boot/"..library, _G)
	if fLib then
		pcall(fLib)
	end
end

--TODO: continue setting up system
print("LoadSound: "..tostring(_audio.loadSound))
local success = _audio.loadSound("repost", "sounds/repost.mp3")
if not success then
	print("Could not load repost.mp3")
else
	print("LOADED SUCCESS!!!")
	for i=0.1, 2, 0.1 do
		print(i)
		_audio.playSound("beep", i, 0, 125)
		timer = system.setTimer(0.125)
		while true do
			local ev, id = coroutine.yield("timer")
			if id == timer then
				--print("GOT TIMER")
				break
			end
		end
	end

	for i=2, 0.1, -0.1 do
		print(i)
		_audio.playSound("beep", i, 0, 125)
		timer = system.setTimer(0.125)
		while true do
			local ev, id = coroutine.yield("timer")
			if id == timer then
				--print("GOT TIMER")
				break
			end
		end
	end
end

while true do
	coroutine.yield()
end
