--[[
		LCOS

	The minimal example OS for LuaComputers.

	NOTE: The current state doesn't really do anything yet,
		wait for later releases.

	TODO: Fix font loading or maybe think of a new font system

	AUTHOR: Piorjade
]]

local _ver = 0.1
local colors = require("colors.lua")
print(tostring(_filesystem.list("/boot")))
print("Colors: "..tostring(colors))

function _G.sleep(timeout)
	local timer = system.setTimer(timeout)
	while true do
		local ev, tim = coroutine.yield()
		if ev == "timer" and tim == timer then
			return
		end
	end
end

--Load core libraries
for each, library in ipairs(_filesystem.list("/boot")) do
	local fLib, sErr = loadfile("/boot/"..library, _G)
	if fLib then
		ok, _err = pcall(fLib)
		if not ok then
			print("[ERROR] "..library..": "..tostring(_err))
		end
	end
end

--TODO: continue setting up system

local c1, err = coroutine.create(shell.run)
if not c1 then return print(err) end

local ev, btn, x, y = nil, nil, nil, nil
while true do
	local ok, err = coroutine.resume(c1, ev, btn, x, y)
	if not ok then return print("RESUME: "..err) end
	ev, btn, x, y = coroutine.yield()
end

--[[_gpu.setColor(colors.magenta)
local pressedBtn = -1
local oldX = 1
local oldY = 1
while true do
	local ev, btn, x, y = coroutine.yield()
	if ev == "mouse_down" and btn == 0 and pressedBtn == -1 then
		pressedBtn = 0
		_gpu.setPixel(x, y)
	elseif ev == "mouse_drag" and pressedBtn == 0 then
		_gpu.setPixel(btn, x)
	elseif ev == "mouse_up" and btn == pressedBtn then
		pressedBtn = -1
	elseif ev == "mouse_up" and pressedBtn == -1 and btn == 1 then
		_gpu.scrollUp(1)
	end

	if ev == "mouse_move" and pressedBtn == -1 then
		_gpu.setColor(colors.black)
		--_gpu.setPixel(oldX, oldY)
		oldX = btn
		oldY = x
		_gpu.setColor(colors.orange)
		_gpu.setPixel(btn, x)
	end

	if ev == "mouse_move" and pressedBtn ~= -1 then
		--_gpu.setColor(colors.black)
		--_gpu.setPixel(oldX, oldY)
		oldX = btn
		oldY = x
	end
end]]
